#include "rip/routines/slam.hpp"

// External
#include <units.h>

// Local
#include "rip/exception_base.hpp"
#include "rip/logger.hpp"

namespace rip::routines
{
    using inch_t       = units::length::inch_t;
    using millimeter_t = units::length::millimeter_t;
    using meter_t      = units::length::meter_t;
    using degree_t     = units::angle::degree_t;

    Slam::Slam(const nlohmann::json& config, std::shared_ptr<EventSystem> es, 
            std::string id, CompTable comps)
        : Routine(config, es, id, comps)
    {
        //lidar->addCallback(std::bind(&Slam::lidarCallback, this, std::placeholders::_1));

        float scan_rate_hz;
        int scan_size;
        degree_t detection_angle;
        inch_t distance_no_detection;
        int detection_margin = 0;
        inch_t offset;

        int map_size_pixels;
        inch_t map_size;

        try
        {
            scan_size             = config["scan_size"].get< int >();
            scan_rate_hz          = config["scan_rate_hz"].get< float >();
            detection_angle       = degree_t(config["detection_angle"].get< float >());
            distance_no_detection = inch_t(config["distance_no_detection"].get< float >());
            detection_margin      = config["detection_margin"].get< int >();
            offset                = inch_t(config["offset"].get< float >());
            map_size_pixels       = config["map_size_pixels"].get< int >();
            map_size              = inch_t(config["map_size"].get< float >());
            m_time_validation     = config["validation_time"].get< double >();
        }
        catch(nlohmann::json::out_of_range& e)
        {
            Logger::error("Lidar Localization Config missing: {}", e.what());
            throw ConfigException("Lidar Localization Config missing: {}", e.what());
        }

        m_laser = Laser(scan_size,
                        scan_rate_hz,
                        detection_angle.value(),
                        millimeter_t(distance_no_detection).value(),
                        detection_margin,
                        millimeter_t(offset).value());
        m_slam  = std::make_unique< RMHC_SLAM >(m_laser, map_size_pixels, meter_t(map_size).value(), 0);
        saveComponents(comps);
    }

    void Slam::start(std::vector<std::any> data)
    {
        m_lidar->start();
        //m_thread = std::make_unique< std::thread >(&Slam::run, this);
        run();
    }

    void Slam::stop(std::vector<std::any> data)
    {
        m_running = false;
        //m_thread->join();
        m_lidar->stop();
    }

    /*void Slam::addCallback(const std::function< void(Pose) >& callback)
    {
        m_callbacks.permanentBind(callback);
    }*/

    /*void Slam::lidarCallback(const std::vector< LaserScan >& scan)
    {
        std::lock_guard< std::mutex > lock(m_mutex);
        m_new_data  = true;
        m_last_scan = scan;
        m_cv.notify_one();
    }*/

    void Slam::run()
    {
        m_running = true;

        while(m_running)
        {
            std::vector<LaserScan> potential_data;
            double diff = 5000;
            const double start_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            while (diff > m_time_validation)
            {
                potential_data = m_lidar->get();
                double timestamp = m_lidar->getTimestamp();
                double curr_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                diff = curr_time - timestamp;
                if (curr_time - start_time > 10 * m_time_validation)
                {
                    throw LidarReadTimeout("Could not read an up-to-date set of data from the Lidar within 10 seconds.");
                }
            }

            std::unique_lock lock(m_mutex);

            m_last_scan = potential_data;

            int* scan = new int[m_last_scan.size()];

            for(int i = 0, end = m_last_scan.size(); i < end; ++i)
            {
                scan[i] = millimeter_t(m_last_scan[i].distance()).value();
            }

            m_slam->update(scan);

            m_pose_mutex.lock();
            m_pose = m_slam->getpos();
            m_pose_mutex.unlock();

            lock.unlock();

            //m_callbacks.trigger(m_pose);
            std::vector<std::any> data;
            data.push_back(get());
            sendMessage(std::string("slam-data"), data);
        }
    }

    void Slam::saveComponents(CompTable comps)
    {
        std::string key;
        try
        {
            key = m_config.at("components").at(0);
        }
        catch (nlohmann::json::type_error &te)
        {
            throw InvalidComponentName("SLAM: Unable to extract the Lidar's name.\n  Name is not a string.");
        }
        catch (nlohmann::json::out_of_range &oor)
        {
            throw InvalidComponentName("SLAM: Unable to extract the Lidar's name.\n  Either the components section of the config was not provided, or there were no components listed in that section.");
        }
        std::shared_ptr<RobotComponent> ptr;
        try
        {
            ptr = comps->at(key);
        }
        catch (std::out_of_range &oor)
        {
            throw ComponentNotFound("Could not find the requested Lidar component");
        }
        m_lidar = std::dynamic_pointer_cast<Lidar>(ptr);
        if (!m_lidar)
        {
            throw ComponentNotFound("Requested component is not a Lidar.");
        }
    }

    void Slam::handle_subscription(std::string handle, std::string func_id, 
            std::string routine_id)
    {
        if (func_id == "stop")
        {
            std::function<void(std::vector<std::any>)> callback = std::bind(&Slam::stop, this,
                    std::placeholders::_1);
            m_esystem->subscribe(handle, callback, routine_id);
        }
        else
        {
            throw InvalidFunctionId("An invalid value was passed to the func_id field in Slam::handle_subscriptions. The only accepted func_id is \"stop\"\n");
        }
    }

    Pose Slam::get()
    {
        std::lock_guard< std::mutex > lock(m_pose_mutex);
        return *(static_cast<Pose*>(&m_pose));
    }

}  // namespace rip
